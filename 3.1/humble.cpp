/*
ID: ruippei1
LANG: C++
TASK: humble
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <set>

#define KMAX 100
#define NMAX 100000

#define MAX_HUMBLE 2147483647

using namespace std;

long primes[KMAX];

int main() {
  ifstream fin("humble.in");
  int k, n;
  fin >> k >> n;

  for (int i = 0; i < k; i++) {
    fin >> primes[i];
  }

  set<long> humbleSet(primes, primes + k);
  int maxHumble = 0; 

  for (int i = 1; i < n; i++) {
    std::set<long>::iterator it = humbleSet.begin();
    long v = *it;
    humbleSet.erase(it);

    for (int j = 0; j < k; j++) {
      if (v <= MAX_HUMBLE / primes[j] && (v * primes[j] < maxHumble || humbleSet.size() < n - i)) {
        humbleSet.insert(v * primes[j]);
        if (v * primes[j] > maxHumble) {
          maxHumble = v * primes[j];
        }
      } else break;
    }
  }
  
  ofstream fout("humble.out");
  fout << *(humbleSet.begin()) << endl;
}