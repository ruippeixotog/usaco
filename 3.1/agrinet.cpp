/*
ID: ruippei1
LANG: C++
TASK: agrinet
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#define NMAX 100

#define INF 10000000

using namespace std;

int edge[NMAX][NMAX];

int main() {
  ifstream fin("agrinet.in");
  int n;
  fin >> n;

  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++) {
      fin >> edge[i][j];
    }
  }

  int dist[NMAX];
  fill(dist, dist + NMAX, INF);
  bool visited[NMAX];
  fill(visited, visited + NMAX, false);

  dist[0] = 0;

  int cost = 0;

  for(int k = 0; k < n; k++) {
    int closerIdx = -1;
    int closerDist = INF;
    for(int i = 0; i < n; i++) {
      if(!visited[i] && dist[i] < closerDist) {
        closerIdx = i;
        closerDist = dist[i];
      }
    }
    visited[closerIdx] = true;
    cost += closerDist;

    for(int i = 0; i < n; i++) {
      if(edge[closerIdx][i] < dist[i]) {
        dist[i] = edge[closerIdx][i];
      }
    }
  }
  
  ofstream fout("agrinet.out");
  fout << cost << endl;
}