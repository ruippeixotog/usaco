/*
ID: ruippei1
LANG: C++
TASK: inflate
*/

#include <iostream>
#include <fstream>
#include <algorithm>

#define MMAX 10000
#define NMAX 10000

using namespace std;

int points[NMAX];
int minutes[NMAX];

int maxPoints[MMAX + 1];

int main() {
  ifstream fin("inflate.in");
  int m, n;
  fin >> m >> n;

  for(int i = 0; i < n; i++) {
    fin >> points[i] >> minutes[i];
  }

  for(int i = 1; i <= m; i++) {
  	for(int j = 0; j < n; j++) {
  	  if(i - minutes[j] >= 0) {
  	  	maxPoints[i] = max(maxPoints[i],
  	  		maxPoints[i - minutes[j]] + points[j]);
  	  }
  	}
  }
  
  ofstream fout("inflate.out");
  fout << maxPoints[m] << endl;
}