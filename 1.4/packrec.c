/*
ID: ruippei1
LANG: C
TASK: packrec
*/

#include <stdio.h>
#include <stdlib.h>

int r[4][2];

int min_area = 9999999;
int num_s = 0;
int s[6][2];

inline int max(int x1, int x2) {
	return x1 > x2 ? x1 : x2;
}

inline int max3(int x1, int x2, int x3) {
	return max(max(x1, x2), x3);
}

inline int max4(int x1, int x2, int x3, int x4) {
	return max(max(x1, x2), max(x3, x4));
}

inline void swap(int* arr, int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

void verify_rect(int w, int h) {
	// printf("%d * %d = %d\n", w, h, w * h);
	if(w * h < min_area) {
		min_area = w * h;
		s[0][0] = w; s[0][1] = h;
		num_s = 1;
	} else if(w * h == min_area) {
		s[num_s][0] = w; s[num_s][1] = h;
		num_s++;
	}
}

void best_rect(int r1, int r2, int r3, int r4) {
	int w, h;

	// case 1
	w = r[r1][0] + r[r2][0] + r[r3][0] + r[r4][0];
	h = max4(r[r1][1], r[r2][1], r[r3][1], r[r4][1]);
	verify_rect(w, h);
	
	// case 2
	w = max(r[r1][0] + r[r2][0] + r[r3][0], r[r4][1]);
	h = max3(r[r1][1], r[r2][1], r[r3][1]) + r[r4][0];
	verify_rect(w, h);

	// case 3
	w = r[r1][0] + max(r[r2][0], r[r3][0]) + r[r4][0];
	h = max3(r[r1][1], r[r2][1] + r[r3][1], r[r4][1]);
	verify_rect(w, h);

	// case 4
	w = max(r[r1][0], r[r2][0]) + r[r3][0] + r[r4][0];
	h = max3(r[r1][1] + r[r2][1], r[r3][1], r[r4][1]);
	verify_rect(w, h);

	// case 5
	w = max(r[r1][0] + r[r2][0], r[r3][1]) + r[r4][0];
	h = max(max(r[r1][1], r[r2][1]) + r[r3][0], r[r4][1]);
	verify_rect(w, h);

	// case 6
	w = max(r[r1][0] + r[r4][1], r[r2][0] + r[r3][0]);
	h = max(r[r1][1] + r[r2][1], r[r3][1] + r[r4][0]);
	verify_rect(w, h);
}

void best_rect_rot(int* perm, int rot) {
	if(rot == 4) {
		best_rect(perm[0], perm[1], perm[2], perm[3]);
		return;
	}
	best_rect_rot(perm, rot + 1);
	swap(r[perm[rot]], 0, 1);
	best_rect_rot(perm, rot + 1);
	swap(r[perm[rot]], 0, 1);
}

void best_rect_perm(int* perm, int k, int m) {
	if(k == m) {
		best_rect_rot(perm, 0);
		return;
	}
	int i;
	for(i = k; i < m; i++) {
		swap(perm, i, m - 1);
		best_rect_perm(perm, k, m - 1);
		swap(perm, i, m - 1);
	}
}

int main () {
	FILE* fin = fopen("packrec.in", "r");

	int i;
    for(i = 0; i < 4; i++) {
		fscanf(fin, "%d %d", &r[i][0], &r[i][1]);
		if(r[i][0] > r[i][1])
			swap(r[i], 0, 1);
		// printf("rect: %d %d", r[i][0], r[i][1]);
	}
	
	int perm[4] = {0,1,2,3};
	best_rect_perm(perm, 0, 4);

	FILE* fout = fopen("packrec.out", "w");
	fprintf(fout, "%d\n", min_area);
	for(i = 0; i < num_s; i++) {
		if(s[i][0] > s[i][1])
			swap(s[i], 0, 1);
		fprintf(fout, "%d %d\n", s[i][0], s[i][1]);
	}
	exit(0);
}


