/*
ID: ruippei1
LANG: C
TASK: milk3
*/

#define N 20

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int ma, mb, mc;
int visited[N+1][N+1];
int valid_c[N+1];

int min(int x, int y) {
	return x > y ? y : x;
}

void dfs(int a, int b, int c) {
	if(visited[b][c]) {
		return;
	}
	visited[b][c] = 1;
	if(a == 0) {
		valid_c[c] = 1;
	}
	if(a > 0) {
		int move = min(mb - b, a);
		dfs(a - move, b + move, c);
		move = min(mc - c, a);
		dfs(a - move, b, c + move);
	}
	if(b > 0) {
		int move = min(ma - a, b);
		dfs(a + move, b - move, c);
		move = min(mc - c, b);
		dfs(a, b - move, c + move);
	}
	if(c > 0) {
		int move = min(ma - a, c);
		dfs(a + move, b, c - move);
		move = min(mb - b, c);
		dfs(a, b + move, c - move);
	}
}

int main() {
	FILE* fin = fopen("milk3.in", "r");
	fscanf(fin, "%d %d %d", &ma, &mb, &mc);

	memset(visited, 0, sizeof(visited));
	memset(valid_c, 0, sizeof(valid_c));
	dfs(0, 0, mc);

	FILE* fout = fopen("milk3.out", "w");
	int i, first = 1;
	for(i = 0; i < mc + 1; i++) {
		if(valid_c[i]) {
			if(first) {
				fprintf(fout, "%d", i);
				first = 0;
			} else {
			fprintf(fout, " %d", i);
			}
		}
	}
	fprintf(fout, "\n");
	exit(0);
}


