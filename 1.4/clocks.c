/*
ID: ruippei1
LANG: C
TASK: clocks
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int solved[9] = {	12,12,12,
						12,12,12,
						12,12,12};

const int moves[9][5] = {	{0,1,3,4,0}, {0,1,2,0,0}, {1,2,4,5,0},
							{0,3,6,0,0}, {1,3,4,5,7}, {2,5,8,0,0},
							{3,4,6,7,0}, {6,7,8,0,0}, {4,5,7,8,0} };

const int move_size[9] = {	4,3,4,
							3,5,3,
							4,3,4	};

int r[9], s[3*9];

void rotate(int i) {
	if(r[i] == 12) {
		r[i] = 3;
	} else {
		r[i] += 3;
	}
}

void unrotate(int i) {
	if(r[i] == 3) {
		r[i] = 12;
	} else {
		r[i] -= 3;
	}
}

void move(int m) {
	int i;
	for(i = 0; i < move_size[m]; i++) {
		rotate(moves[m][i]);
	}
}

void unmove(int m) {
	int i;
	for(i = 0; i < move_size[m]; i++) {
		unrotate(moves[m][i]);
	}
}

/*void print_clocks(int* r) {
	int i;
    for(i = 0; i < 3; i++) {
		printf("%d %d %d\n", r[3*i+0], r[3*i+1], r[3*i+2]);
	}
	printf("\n");
}*/

/*int dfs(int* r, int depth, int max_d, int* s) {
	if(depth == max_d) {
		return memcmp(r, solved, sizeof(solved)) == 0;
	}
	int i;
	for(i = 0; i < 9; i++) {
		move(r, i);
		// print_clocks(r);
		if(dfs(r, depth + 1, max_d, s)) {
			s[depth] = i + 1;
			return 1;
		}
		unmove(r, i);
	}
	return 0;
}*/

int dfs(int depth, int max_d, int curr_m, int num_m) {
	if(depth == max_d) {
		return memcmp(r, solved, sizeof(solved)) == 0;
	}

	if(num_m < 3) {
		move(curr_m);
		if(dfs(depth + 1, max_d, curr_m, num_m + 1)) {
			s[depth] = curr_m + 1;
			return 1;
		}
		unmove(curr_m);
	}
	int i;
	for(i = curr_m + 1; i < 9; i++) {
		move(i);
		// print_clocks(r);
		if(dfs(depth + 1, max_d, i, 0)) {
			s[depth] = i + 1;
			return 1;
		}
		unmove(i);
	}
	return 0;
}

int main() {
	FILE* fin = fopen("clocks.in", "r");

	int i;
    for(i = 0; i < 3; i++) {
		fscanf(fin, "%d %d %d", &r[3*i+0], &r[3*i+1], &r[3*i+2]);
	}

	int solved = 0, depth = 0;
	while(!solved) {
		solved = dfs(0, depth++, 0, 0);
	}

	FILE* fout = fopen("clocks.out", "w");
	if(depth > 1) {
		fprintf(fout, "%d", s[0]);
	}
	for(i = 1; i < depth - 1; i++) {
		fprintf(fout, " %d", s[i]);
	}
	fprintf(fout, "\n");
	exit(0);
}


