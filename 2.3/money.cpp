/*
ID: ruippei1
LANG: C++
TASK: money
*/

#include <iostream>
#include <fstream>
#include <cstring>

#define VMAX 25
#define NMAX 10000

using namespace std;

int coins[VMAX];
int n, v;

long long dp[NMAX + 1];

int main() {
  ifstream fin("money.in");
  fin >> v >> n;
  for(int i = 0; i < v; i++) {
    fin >> coins[i];
  }

  memset(dp, 0, sizeof(dp));
  dp[0] = 1;

  for(int i = 0; i < v; i++) {
    for(int j = coins[i]; j <= n; j++) {
      dp[j] += dp[j - coins[i]];
    }
  }
  
  ofstream fout("money.out");
  fout << dp[n] << endl;
}