/*
ID: ruippei1
LANG: C++
TASK: concom
*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <map>
#include <set>

#define CMAX 100

using namespace std;

struct Controlling {
  int numCompanies;
  int owner[CMAX];
  int percent[CMAX][CMAX];
  map<int, int> pending;

  Controlling(): numCompanies(0) {
    for(int i = 0; i < CMAX; i++) {
      owner[i] = i;
    }
    memset(percent, 0, sizeof(percent));
  }

  void add(int i, int j, int p) {
    percent[i][j] += p;
    if(i >= numCompanies) numCompanies = i + 1;
    if(j >= numCompanies) numCompanies = j + 1;
    if(p > 50) pending[j] = i;
  }

  void addAll(int i, int j) {
    for(int k = 0; k < numCompanies; k++) {
      percent[i][k] += percent[j][k];
      if(i != k && percent[i][k] > 50 && owner[k] == k && !pending.count(k)) {
        pending[k] = i;
      }
    }
  }

  void takeAll(int i, int j) {
    owner[j] = i;
    addAll(i, j);

    set<int> processed;
    processed.insert(i);
    while(owner[i] != i && !processed.count(owner[i])) {
      i = owner[i];
      addAll(i, j);
      processed.insert(i);
    }
  }

  void process() {
    while(!pending.empty()) {
      map<int, int>::iterator it = pending.begin();
      takeAll(it->second, it->first);
      pending.erase(it);
    }
  }
};

int main() {
  ifstream fin("concom.in");
  int n;
  fin >> n;

  Controlling controls;

  for(int i = 0; i < n; i++) {
    int i, j, p;
    fin >> i >> j >> p;
    controls.add(i - 1, j - 1, p);
  }

  controls.process();
  
  ofstream fout("concom.out");
  for(int i = 0; i < controls.numCompanies; i++) {
    for(int j = 0; j < controls.numCompanies; j++) {
      if(i != j && controls.percent[i][j] > 50) {
        fout << (i + 1) << " " << (j + 1) << endl;
      }
    }
  }
}