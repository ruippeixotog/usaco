/*
ID: ruippei1
LANG: C++
TASK: prefix
*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <map>

#define SMAX 200000

using namespace std;

struct Tree {
  bool isPrimitive;
  map<char, Tree> children;

  Tree(): isPrimitive(false) {}

  void add(string str) {
    if(str.empty()) isPrimitive = true;
    else children[str[str.length() - 1]].add(str.substr(0, str.length() - 1));
  }
};

bool valid[SMAX + 1];

bool isValid(Tree& tree, string& seq, int pos) {
  if(valid[pos] && tree.isPrimitive) return true;
  else if(pos == 0 || !tree.children.count(seq[pos - 1])) return false;
  else return isValid(tree.children[seq[pos - 1]], seq, pos - 1);
}

int main() {
  Tree tree;

  ifstream fin("prefix.in");
  string prim = "";
  do {
    tree.add(prim);
    fin >> prim;
  } while(prim != ".");

  string seq;
  while (!fin.eof()) {
    string line;
    fin >> line;
    seq += line;
  }

  valid[0] = true;
  int bestValid = 0;

  for(unsigned int i = 1; i <= seq.length(); i++) {
    valid[i] = false;
    if(isValid(tree, seq, i)) {
      valid[i] = true;
      bestValid = i;
    }
  }
  
  ofstream fout("prefix.out");
  fout << bestValid << endl;
}