/*
ID: ruippei1
LANG: C
TASK: nocows
*/

/*
S(1,1) = 1
S(n,k) = 0, k > n
S(n,k) = 2 * sum(i=[1,3,...,n/2], 	S(i, k-1) * S(n-i-1, k-1) +
									S(i, k-1) * sum(j=[0,k-1], S(n-i-1, j)) +
									S(n-i-1, k-1) * sum(j=[0,k-1], S(i, j))  )
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXN 199
#define MAXK 99

int tmem[MAXN][MAXK];
int tacum[MAXN][MAXK];

inline int tacum_g(int n, int k) {
	return k > n ? tacum[n][n] : tacum[n][k];
}

long num_trees(int n, int k) {
	int i, j, left_n;
	for(i = 3; i <= n; i += 2) {
		// tmem[i][1] is always 0 for i >= 3
		for(j = 2; j <= i; j++) {
			int sum = 0;
			for(left_n = 1; left_n <= (i-1) / 2; left_n += 2) {
				int div_sum = 0;
				int right_n = i - left_n - 1;
				div_sum += tmem[left_n][j-1] * tmem[right_n][j-1];
				div_sum += tmem[left_n][j-1] * tacum_g(right_n, j-2);
				div_sum += tacum_g(left_n, j-2) * tmem[right_n][j-1];
				if(left_n != (i-1) / 2) {
					div_sum *= 2;
				}
				sum += div_sum % 9901;
			}
			tmem[i][j] = sum % 9901;
			tacum[i][j] = (tacum[i][j-1] + tmem[i][j]) % 9901;
		}
	}
	return tmem[n][k];
}

int main() {
	FILE* fin = fopen("nocows.in", "r");
	FILE* fout = fopen("nocows.out", "w");
	
	memset(tmem, 0, sizeof(tmem));
	memset(tacum, 0, sizeof(tacum));
	tmem[1][1] = tacum[1][1] = 1;
	
	int n, k;
	fscanf(fin, "%d %d", &n, &k);
	fprintf(fout, "%ld\n", num_trees(n, k));
	
	exit(0);
}
