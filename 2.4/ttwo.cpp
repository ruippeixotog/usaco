/*
ID: ruippei1
LANG: C++
TASK: ttwo
*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>

#define N 10

using namespace std;

string field[N];
bool visited[N][N][4][N][N][4];

int dx[] = {0, 1, 0, -1};
int dy[] = {-1, 0, 1, 0};

void move(int& y, int& x, int& dir) {
  int yNext = y + dy[dir];
  int xNext = x + dx[dir];
  if(yNext < 0 || yNext >= N ||
    xNext < 0 || xNext >= N ||
    field[yNext][xNext] == '*') {
    dir = (dir + 1) % 4;
  } else {
    y = yNext;
    x = xNext;
  }
} 

int main() {
  int fDir = 0, cDir = 0;
  int fy, fx, cy, cx;

  ifstream fin("ttwo.in");
  for(int i = 0; i < N; i++) {
    getline(fin, field[i]);

    for(int j = 0; j < N; j++) {
      if(field[i][j] == 'F') {
        fy = i; fx = j;
      } else if(field[i][j] == 'C') {
        cy = i; cx = j;
      }
    }
  }

  memset(visited, false, sizeof(visited));
  visited[fy][fx][fDir][cy][cx][cDir] = true;

  int t = 0;
  while(fy != cy || fx != cx) {
    move(fy, fx, fDir);
    move(cy, cx, cDir);
    if(visited[fy][fx][fDir][cy][cx][cDir]) {
      t = 0;
      break;
    }
    visited[fy][fx][fDir][cy][cx][cDir] = true;
    t++;
  }
  
  ofstream fout("ttwo.out");
  fout << t << endl;
}