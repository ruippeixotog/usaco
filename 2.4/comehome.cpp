/*
ID: ruippei1
LANG: C++
TASK: comehome
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#define VMAX 52
#define PMAX 10000

#define INF 10000000

using namespace std;

inline int toInt(char ch) {
  if(ch <= 'Z') return ch - 'A';
  else return (ch - 'a') + 26;
}

struct Edge {
  int v1, v2; int d;

  Edge() {}
  Edge(char v1ch, char v2ch, int da): d(da) {
    v1 = toInt(v1ch);
    v2 = toInt(v2ch);
  }
};

Edge edges[PMAX];

int main() {
  ifstream fin("comehome.in");
  int p;
  fin >> p;

  for(int i = 0; i < p; i++) {
    char v1, v2; int d;
    fin >> v1 >> v2 >> d;
    edges[i] = Edge(v1, v2, d);
  }

  int dist[VMAX];
  fill(dist, dist + VMAX, INF);
  dist[toInt('Z')] = 0;

  for(int k = 0; k < VMAX; k++) {
    for(int j = 0; j < p; j++) {
      Edge& e = edges[j];
      if(dist[e.v1] + e.d < dist[e.v2]) {
        dist[e.v2] = dist[e.v1] + e.d;
      }
      if(dist[e.v2] + e.d < dist[e.v1]) {
        dist[e.v1] = dist[e.v2] + e.d;
      }
    }
  }

  int bestIdx = -1;
  int bestDist = INF;
  for(int i = 0; i < 25; i++) {
    if(dist[i] < bestDist) {
      bestDist = dist[i];
      bestIdx = i;
    }
  }
  
  ofstream fout("comehome.out");
  fout << ((char)('A' + bestIdx)) << " " << bestDist << endl;
}