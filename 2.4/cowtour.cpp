/*
ID: ruippei1
LANG: C++
TASK: cowtour
*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>

#include <cstdio>
#include <cmath>
#include <limits>
#include <algorithm>

#define NMAX 150

using namespace std;

const int INF = numeric_limits<double>::infinity();

int points[NMAX][2];
double dist[NMAX][NMAX];
double maxDist[NMAX];

int group[NMAX];
double diameter[NMAX];

double calcDist(int i, int j) {
  int dx = points[i][0] - points[j][0];
  int dy = points[i][1] - points[j][1];
  return sqrt(dx * dx + dy * dy);
}

void calcAllShortestPaths(int n) {
  for(int k = 0; k < n; k++) {
    for(int i = 0; i < n; i++) {
      for(int j = 0; j < n; j++) { 
        if(dist[i][k] + dist[k][j] < dist[i][j]) {
          dist[i][j] = dist[i][k] + dist[k][j];
        }
      }
    }
  }
}

void calcMaxDists(int n) {
  memset(maxDist, 0.0, sizeof(maxDist));

  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++) {
      if(dist[i][j] != INF) {
        maxDist[i] = max(maxDist[i], dist[i][j]);
      }
    }
  }
}

int findGroups(int n) {
  memset(group, -1, sizeof(group));
  memset(diameter, 0.0, sizeof(diameter));

  int numGroups = 0;
  for(int i = 0; i < n; i++) {
    if(group[i] == -1) {
      int g = group[i] = numGroups++;
      diameter[g] = maxDist[i];

      for(int j = 0; j < n; j++) {
        if(dist[i][j] != INF) {
          group[j] = g;
          diameter[g] = max(diameter[g], maxDist[j]);
        }
      }
    }
  }
  return numGroups;
}

double bestJoinDiameter(int n) {
  double bestDiameter = INF;
  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++) {
      if(group[i] != group[j]) {
        double jointDiameter = maxDist[i] + maxDist[j] + calcDist(i, j);
        jointDiameter = max(jointDiameter, diameter[group[i]]);
        jointDiameter = max(jointDiameter, diameter[group[j]]);
        bestDiameter = min(bestDiameter, jointDiameter);
      }
    }
  }
  return bestDiameter;
}

int main() {
  ifstream fin("cowtour.in");
  int n;
  fin >> n;

  for(int i = 0; i < n; i++) {
    fin >> points[i][0] >> points[i][1];
  }

  for(int i = 0; i < n; i++) {
    string adj;
    fin >> adj;
    for(int j = 0; j < n; j++) {
      if(i == j) dist[i][j] = 0.0;
      else if(adj[j] == '0') dist[i][j] = INF;
      else dist[i][j] = calcDist(i, j);
    }
  }

  calcAllShortestPaths(n);
  calcMaxDists(n);
  findGroups(n);
  double bestDiameter = bestJoinDiameter(n);
  
  FILE* fout = fopen("cowtour.out", "w");
  fprintf(fout, "%.6f\n", bestDiameter);
}