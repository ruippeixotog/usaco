/*
ID: ruippei1
LANG: C++
TASK: maze1
*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <queue>

#define WMAX 38
#define HMAX 100

using namespace std;

int w, h;
vector<string> maze;

bool visited[HMAX][WMAX];

struct State {
  int row, col, dist;
  State(int i, int j, int d): row(i), col(j), dist(d) {}

  bool up() { return maze[row * 2][col * 2 + 1] == ' '; }
  bool down() { return maze[row * 2 + 2][col * 2 + 1] == ' '; }
  bool left() { return maze[row * 2 + 1][col * 2] == ' '; }
  bool right() { return maze[row * 2 + 1][col * 2 + 2] == ' '; }

  bool end() { return row < 0 || col < 0 || row >= h || col >= w; }
};

int distanceToEnd(int i, int j) {
  queue<State> states;
  states.push(State(i, j, 0));
  memset(visited, false, sizeof(visited));

  while(!states.empty()) {
    State s = states.front();
    states.pop();

    if(s.end()) return s.dist;
    if(visited[s.row][s.col]) continue;
    visited[s.row][s.col] = true;

    if(s.up()) states.push(State(s.row - 1, s.col, s.dist + 1));
    if(s.down()) states.push(State(s.row + 1, s.col, s.dist + 1));
    if(s.left()) states.push(State(s.row, s.col - 1, s.dist + 1));
    if(s.right()) states.push(State(s.row, s.col + 1, s.dist + 1));
  }
  // cout << "Exit not found from " << i << " " << j << endl;
  return 99999999; // infinity
}

int main() {
  ifstream fin("maze1.in");
  fin >> w >> h;

  string row;
  getline(fin, row);
  for(int i = 0; i < h * 2 + 1; i++) {
    getline(fin, row);
    maze.push_back(row);
  }

  int maxDist = 0;
  for(int i = 0; i < h; i++) {
    for(int j = 0; j < w; j++) {
      int dist = distanceToEnd(i, j);
      if(dist > maxDist) {
        maxDist = dist;
      }
    }
  }
  
  ofstream fout("maze1.out");
  fout << maxDist << endl;
}