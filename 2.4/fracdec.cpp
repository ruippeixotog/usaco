/*
ID: ruippei1
LANG: C++
TASK: fracdec
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#include <sstream>

#define DMAX 100000

using namespace std;

int main() {
  ifstream fin("fracdec.in");
  int n, d;
  fin >> n >> d;

  int intPart = n / d;
  n -= intPart * d;

  string remPart;

  int visited[DMAX];
  fill(visited, visited + DMAX, -1);

  for(int i = 0; i < DMAX; i++) {
    visited[n] = i;

    n *= 10;
    int digit = n / d;
    n -= digit * d;
    remPart += '0' + digit;
    
    if(n == 0 || visited[n] != -1) break;
  }

  stringstream ss;
  ss << intPart << ".";
  if(n == 0) {
    ss << remPart;
  } else {
    ss << remPart.substr(0, visited[n]) << "(" <<
      remPart.substr(visited[n]) << ")";
  }

  string result = ss.str();
  ofstream fout("fracdec.out");
  for(unsigned int i = 0; i < result.size(); i += 76) {
    fout << result.substr(i, 76) << endl;
  }
}