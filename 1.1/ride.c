/*
ID: ruippei1
LANG: C
TASK: ride
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char comet[7], group[7];

int main () {
	FILE* fin = fopen("ride.in", "r");
	fscanf(fin, "%s", comet);
	fscanf(fin, "%s", group);
	
	int i;
	int clen = strlen(comet), cmult = 1;
	for(i = 0; i < clen; i++) {
		cmult *= comet[i] - 'A' + 1;
	}

	int glen = strlen(group), gmult = 1;
	for(i = 0; i < glen; i++) {
		gmult *= group[i] - 'A' + 1;
	}

	FILE* fout = fopen("ride.out", "w");
	if(cmult % 47 == gmult % 47) {
		fprintf(fout, "%s\n", "GO");
	} else {
		fprintf(fout, "%s\n", "STAY");
	}
	exit(0);
}