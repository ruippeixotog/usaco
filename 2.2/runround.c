/*
ID: ruippei1
LANG: C
TASK: runround
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int m;

char digit_used[9] = {0,0,0,0,0,0,0,0,0};

char curr_str[9];
int curr_size;

FILE* fout;

void swap(char* arr, int i, int j) {
	char temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

void test_runround() {
	char visited[9] = {0,0,0,0,0,0,0,0,0};

	int nvisited = 0;
	int i = 0;
	while(!visited[i]) {
		visited[i] = 1;
		nvisited++;
		i = (i + (curr_str[i] - '0')) % curr_size;
	}
	if(i == 0 && nvisited == curr_size) {
		int runround = 0;
		sscanf(curr_str, "%d", &runround);
		// printf("runround: %d (%d)\n", runround, m);
		if(runround > m) {
			fprintf(fout, "%d\n", runround);
			exit(0);
		}
	}
}

void gen_rec(int k) {
	if(k == curr_size) {
		test_runround();
		return;
	}
	int i;
	for(i = 1; i <= 9; i++) {
		if(!digit_used[i]) {
			curr_str[k] = '0' + i;
			digit_used[i] = 1;
			gen_rec(k + 1);
			digit_used[i] = 0;
		}
	}
}

void gen(int n) {
	curr_size = n;
	digit_used[n] = 1;
	gen_rec(0);
	digit_used[n] = 0;
}

int main() {
	FILE* fin = fopen("runround.in", "r");
	fout = fopen("runround.out", "w");
	fscanf(fin, "%d", &m);

	int size = sprintf(curr_str, "%d", m);

	int i;
	for(i = size; i < 9; i++) {
		gen(i);
	}
	exit(0);
}
