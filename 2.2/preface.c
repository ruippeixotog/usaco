/*
ID: ruippei1
LANG: C
TASK: preface
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int n;

char letters[7] = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
int cnt[9];

int to_add[10][3] = {	{0,0,0}, {1,0,0}, {2,0,0}, {3,0,0},
						{1,1,0}, {0,1,0}, {1,1,0},
						{2,1,0}, {3,1,0}, {1,0,1}};

int acum[10][3] = {		{0,0,0}, {1,0,0}, {3,0,0}, {6,0,0},
						{7,1,0}, {7,2,0}, {8,3,0},
						{10,4,0}, {13,5,0}, {14,5,1}};

int main() {
	FILE* fin = fopen("preface.in", "r");
	FILE* fout = fopen("preface.out", "w");
	fscanf(fin, "%d", &n);

	memset(cnt, 0, sizeof(cnt));

	int i, order = 1000;
	int higher = 0;
	for(i = 6; i >= 0; i -= 2, order /= 10) {
		cnt[i] += 14 * higher * order;
		cnt[i+1] += 5 * higher * order;
		cnt[i+2] += higher * order;

		int digit = n / order;
		if(digit != 0) {
			int* acum_v = acum[digit - 1];
			cnt[i] += acum_v[0] * order;
			cnt[i+1] += acum_v[1] * order;
			cnt[i+2] += acum_v[2] * order;

			int rem = n % order + 1;
			int* dig_v = to_add[digit];
			cnt[i] += dig_v[0] * rem;
			cnt[i+1] += dig_v[1] * rem;
			cnt[i+2] += dig_v[2] * rem;

			n -= digit * order;
		}
		higher = higher * 10 + digit;
	}

	for(i = 0; i < 7; i++) {
		if(cnt[i] != 0) {
			fprintf(fout, "%c %d\n", letters[i], cnt[i]);
		}
	}
	exit(0);
}