/*
ID: ruippei1
LANG: C
TASK: subset
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXN 39

int n;
unsigned int lookup[MAXN*(MAXN+1)/4 + 1];

int main() {
	FILE* fin = fopen("subset.in", "r");
	FILE* fout = fopen("subset.out", "w");

	fscanf(fin, "%d", &n);

	int sum = n*(n+1);
	if(sum % 4 != 0) {
		fprintf(fout, "0\n");
		exit(0);
	}
	sum /= 4;

	memset(lookup, 0, sizeof(lookup));
	lookup[0] = 1;

	int i, j;
	for(i = 1; i <= n; i++) {
		for(j = sum - i; j >= 0; j--) {
			lookup[j+i] += lookup[j];
		}
	}

	fprintf(fout, "%d\n", lookup[sum] / 2);
	exit(0);
}