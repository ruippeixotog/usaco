/*
ID: ruippei1
LANG: C++
TASK: lamps
*/

#include <cstdio>
#include <cstdlib>

#include <cstring>

// #include <bitset>

#define MAXN 100

using namespace std;

int n, c;

char rests[MAXN];
bool rest_val[MAXN];
int nrests = 0;

bool on[MAXN];

bool sol[7][MAXN];
int nsol = 0;

void apply1() {
	for(int i = 0; i < n; i++) {
		on[i] = !on[i];
	}
}

void apply2() {
	for(int i = 0; i < n; i += 2) {
		on[i] = !on[i];
	}
}

void apply3() {
	for(int i = 1; i < n; i += 2) {
		on[i] = !on[i];
	}
}

void apply4() {
	for(int i = 0; i < n; i += 3) {
		on[i] = !on[i];
	}
}

bool validate() {
	for(int i = 0; i < nrests; i++) {
		if(on[rests[i]-1] != rest_val[i]) {
			return false;
		}
	}
	memcpy(sol[nsol++], on, n * sizeof(bool));
	return true;
}

void gen() {
	// 1+3=2; 1+2=3; 2+3=1
	memset(on, true, n * sizeof(bool));
	if(c != 1) validate();	// {{}, {1,2,3}}
	if(c == 0) {
		return;
	}
	apply1(); validate();	// {{1}, {2,3}}
	apply3(); validate();	// {{1,3}, {2}}
	apply1(); validate();	// {{3}, {1,2}}
	
	apply4();
	if(c > 1) validate();	// {{3,4}, {1,2,4}}
	apply1();
	if(c > 1) validate();	// {{1,3,4}, {2,4}}
	apply3();
	if(c > 1) validate();	// {{1,4}, {2,3,4}}

	apply1();
	if(c != 2) validate();	// {{4}, {1,2,3,4}}
}

int bitset_cmp(const void* lhs_p, const void* rhs_p) {
	bool* lhs = (bool*) lhs_p;
	bool* rhs = (bool*) rhs_p;
	for(int i = 0; i < n; i++) {
		if(lhs[i] != rhs[i]) {
			return lhs[i] ? 1 : -1;
		}
	}
	return 0;
}

int main() {
	FILE* fin = fopen("lamps.in", "r");
	FILE* fout = fopen("lamps.out", "w");
	fscanf(fin, "%d", &n);
	fscanf(fin, "%d", &c);

	int lamp;
	fscanf(fin, "%d", &lamp);
	while(lamp != -1) {
		rests[nrests] = lamp;
		rest_val[nrests++] = true;
		fscanf(fin, "%d", &lamp);
	}
	fscanf(fin, "%d", &lamp);
	while(lamp != -1) {
		rests[nrests] = lamp;
		rest_val[nrests++] = false;
		fscanf(fin, "%d", &lamp);
	}

	gen();
	qsort(sol, nsol, MAXN * sizeof(bool), bitset_cmp);

	if(nsol == 0) {
		fprintf(fout, "IMPOSSIBLE\n");
	} else {
		for(int i = 0; i < nsol; i++) {
			for(int j = 0; j < n; j++) {
				fprintf(fout, "%d", sol[i][j]);
			}
			fprintf(fout, "\n");
		}
	}
	exit(0);
}