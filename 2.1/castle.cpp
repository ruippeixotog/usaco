/*
ID: ruippei1
LANG: C++
TASK: castle
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>

struct Conn {
	int target, line, col, dir;

	Conn() : target(-1), line(-1), col(-1), dir(-1) {}
	Conn(int t, int l, int c, int d) : target(t), line(l), col(c), dir(d) {}

	bool operator<(Conn& val) {
		if(col != val.col) {
			return col > val.col;
		}
		if(line != val.line) {
			return line < val.line;
		}
		return dir > val.dir;
	}
};

int w, h;

int map[50][50];
int ids[50][50];

int id_size[50*50+1];
int id_idx = 1;
Conn connects[50*50+1];

void add_conn(int id1, int id2, int line, int col, int dir) {
	Conn new_conn = Conn(id1, line, col, dir);
	if(connects[id2].line == -1) {
		connects[id2] = new_conn;
	} else if(id_size[id1] > id_size[connects[id2].target]) {
		// printf("New conn: %d to %d @ (%d, %d)\n", id1, id2, line, col);
		connects[id2] = new_conn;
	} else if(id_size[id1] == id_size[connects[id2].target] &&
				connects[id2] < new_conn) {
		connects[id2] = new_conn;
	}
}

int check_valid_id(int line, int col, int curr_id) {
	if(line < 0 || col < 0 || line >= h || col >= w) {
		return 0;
	}
	int id = ids[line][col];
	return id && id != curr_id;
}

int dfs(int line, int col, int id) {
	if(ids[line][col]) {
		return 0;
	}
	ids[line][col] = id;

	int count = 1;
	int walls = map[line][col];

	if(walls & 0x01) { // west
		if(check_valid_id(line, col-1, id)) {
			add_conn(ids[line][col-1], id, line, col-1, 0x04);
		}
	} else {
		count += dfs(line, col-1, id);
	}

	if(walls & 0x02) { // north
		if(check_valid_id(line-1, col, id)) {
			add_conn(ids[line-1][col], id, line, col, 0x02);
		}
	} else {
		count += dfs(line - 1, col, id);
	}

	if(walls & 0x04) { // east
		if(check_valid_id(line, col+1, id)) {
			add_conn(ids[line][col+1], id, line, col, 0x04);
		}
	} else {
		count += dfs(line, col + 1, id);
	}

	if(walls & 0x08) { // south
		if(check_valid_id(line+1, col, id)) {
			add_conn(ids[line+1][col], id, line+1, col, 0x02);
		}
	} else {
		count += dfs(line + 1, col, id);
	}
	return count;
}

int main() {
	FILE* fin = fopen("castle.in", "r");
	fscanf(fin, "%d %d", &w, &h);
	int i, j;
	for(i = 0; i < h; i++) {
		for(j = 0; j < w; j++) {
			fscanf(fin, "%d", &map[i][j]);
		} 
	}

	memset(ids, 0, sizeof(ids));
	connects[1] = Conn();

	int largest_room = -1;
	for(int i = 0; i < h; i++) {
		for(int j = 0; j < w; j++) {
			int count = dfs(i, j, id_idx);
			if(count > 0) {
				// printf("New room %d: size %d, start @ (%d, %d)\n", id_idx, count, i, j);
				id_size[id_idx++] = count;
				connects[id_idx] = Conn();
				if(count > largest_room) {
					largest_room = count;
				}
			}
		}
	}
	id_idx--;

	int join_best_size = -1;
	Conn best_conn;
	for(int i = 1; i <= id_idx; i++) {
		Conn connect = connects[i];
		// printf("%d: conn to %d @ (%d,%d)\n", i, connect.target, connect.line, connect.col);
		if(connect.line == -1) {
			continue;
		}
		int join_size = id_size[i] + id_size[connect.target];
		if(join_size > join_best_size ||
			(join_size == join_best_size && best_conn < connect)) {
			join_best_size = join_size;
			best_conn = connect;
		}
	}

	FILE* fout = fopen("castle.out", "w");
	fprintf(fout, "%d\n", id_idx);
	fprintf(fout, "%d\n", largest_room);
	fprintf(fout, "%d\n", join_best_size);
	fprintf(fout, "%d %d %c\n", best_conn.line + 1,
								best_conn.col + 1,
								best_conn.dir == 0x02 ? 'N' : 'E');
	exit(0);
}
