/*
ID: ruippei1
LANG: C
TASK: holstein
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int nv, nf;

int vreq[25];
int feeds[15][25];

int best_food[15];
int bcount = 999999;

int curr_food[15];
int curr_vit[25];
int fcount = 0;

int add_food(int f) {
	int i, filled = 1;
	for(i = 0; i < nv; i++) {
		curr_vit[i] += feeds[f][i];
		if(curr_vit[i] < vreq[i]) {
			filled = 0;
		}
	}
	curr_food[fcount++] = f;
	return filled;
}

void remove_food(int f) {
	int i;
	for(i = 0; i < nv; i++) {
		curr_vit[i] -= feeds[f][i];
	}
	fcount--;
}

int bt(int f) {
	if(add_food(f)) {
		if(fcount < bcount) {
			bcount = fcount;
			memcpy(best_food, curr_food, fcount * sizeof(int));
		}
		remove_food(f);
		return 1;
	}
	int i;
	for(i = f + 1; i < nf; i++) {
		if(bt(i)) {
			break;
		}
	}
	remove_food(f);
	return 0;
}

int main() {
    FILE* fin  = fopen("holstein.in", "r");
    FILE* fout = fopen("holstein.out", "w");
	fscanf(fin, "%d", &nv);
	int i, j;
	for(i = 0; i < nv; i++) {
		fscanf(fin, "%d", &vreq[i]);
	}
	fscanf(fin, "%d", &nf);
	for(i = 0; i < nf; i++) {
		for(j = 0; j < nv; j++) {
			fscanf(fin, "%d", &feeds[i][j]);
		}
	}

	memset(curr_vit, 0, sizeof(curr_vit));
	for(i = 0; i < nf; i++) {
		bt(i);
	}

    fprintf(fout, "%d", bcount);
	for(i = 0; i < bcount; i++) {
		fprintf(fout, " %d", best_food[i] + 1);
	}
	fprintf(fout, "\n");
    exit (0);
}
