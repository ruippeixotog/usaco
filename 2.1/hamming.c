/*
ID: ruippei1
LANG: C
TASK: hamming
*/

#include <stdio.h>
#include <stdlib.h>

typedef unsigned char uchar;

int n, b, d;

int max_w;

int s[64];
int s_idx = 0;


int hdist(char n1, char n2) {
	int n_xor = n1 ^ n2;
	int count = 0;
	int i;
	for(i = 0; i < b; i++) {
		if(n_xor & (1 << i)) {
			count++;
		}
	}
	return count;
}

int main() {
	FILE* fin = fopen("hamming.in", "r");
	fscanf(fin, "%d %d %d", &n, &b, &d);

	max_w = (1 << b) - 1;
	int i, j;
	for(i = 0; i <= max_w; i++) {
		for(j = s_idx - 1; j >= 0; j--) {
			if(hdist(i, s[j]) < d) {
				break;
			}
		}
		if(j == -1) {
			s[s_idx++] = i;
			if(s_idx == n) {
				break;
			}
		}
	}

	FILE* fout = fopen("hamming.out", "w");
	fprintf(fout, "0");
	for(i = 1; i < s_idx; i++) {
		if(i % 10 == 0) {
			fprintf(fout, "\n%d", s[i]);
		} else {
			fprintf(fout, " %d", s[i]);
		}
	}
	fprintf(fout, "\n");
	exit(0);
}
