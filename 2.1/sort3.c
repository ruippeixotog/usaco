/*
ID: ruippei1
LANG: C
TASK: sort3
*/

#include <stdio.h>
#include <stdlib.h>

int n;
int seq[1000];
int cnt[4] = {0,0,0,0};

int mcount = 0;

inline int in_place(int val, int idx) {
	return idx >= cnt[val-1] && idx < cnt[val];
}

void swap(int* arr, int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
	mcount++;
	// printf("swap %d with %d\n", i, j);
}

void triple_move() {
	int i, j, k;
	for(i = 0; i < n; i++) {
		if(!in_place(seq[i], i)) {
			for(j = cnt[seq[i]-1]; j < cnt[seq[i]]; j++) {
				int found = 0;
				if(!in_place(seq[j], j)) {
					for(k = cnt[seq[j]-1]; k < cnt[seq[j]]; k++) {
						if(in_place(seq[k], i)) {
							swap(seq, i, j);
							swap(seq, i, k);
							found = 1;
							break;
						}
					}
				} 
				if(found) break;
			}
		}
	}
}

void simple_swap() {
	int i, j;
	for(i = 0; i < n; i++) {
		if(!in_place(seq[i], i)) {
			for(j = cnt[seq[i]-1]; j < cnt[seq[i]]; j++) {
				if(in_place(seq[j], i)) {
					swap(seq, i, j);
					break;
				} 
			}
		}
	}
}

int main() {
	FILE* fin = fopen("sort3.in", "r");
	fscanf(fin, "%d", &n);
	int i;
	for(i = 0; i < n; i++) {
		fscanf(fin, "%d", &seq[i]);
		cnt[seq[i]]++;
	}
	cnt[2] += cnt[1];
	cnt[3] += cnt[2];
	FILE* fout = fopen("sort3.out", "w");

	simple_swap();
	triple_move();

	fprintf(fout, "%d\n", mcount);
	exit(0);
}
