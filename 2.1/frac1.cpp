/*
ID: ruippei1
LANG: C++
TASK: frac1
*/

#include <cstdio>
#include <cstdlib>

#include <set>

using namespace std;

int n;

struct Frac {
	int num, denom;
	unsigned int comp;

	Frac(int n, int d) : num(n), denom(d) {
		comp = n * 100000 / denom;
	}

	bool operator<(const Frac& val) const {
		return comp < val.comp;
	}

	bool operator==(const Frac& val) const {
		return comp == val.comp;
	}
};

set<Frac> fset;

int main () {
	FILE* fin = fopen("frac1.in", "r");
	fscanf(fin, "%d", &n);

	if(n > 0) {
		fset.insert(Frac(0, 1));
		fset.insert(Frac(1, 1));
	}
	for(int d = 2; d <= n; d++) {
		for(int num = 1; num < d; num++) {
			fset.insert(Frac(num, d));
		}
	}

	FILE* fout = fopen("frac1.out", "w");
	for(set<Frac>::const_iterator it = fset.begin(); it != fset.end(); it++) {
		fprintf(fout, "%d/%d\n", it->num, it->denom);
	}
	exit(0);
}