/*
ID: ruippei1
LANG: C
TASK: numtri
*/

#include <stdio.h>
#include <stdlib.h>

int r[1000][1000];

inline int max(int x1, int x2) {
	return x1 > x2 ? x1 : x2;
}

int main () {
	FILE* fin = fopen("numtri.in", "r");
	int lines;
	fscanf(fin, "%d", &lines);

	int i, l;
	for(l = 0; l < lines; l++) {
		for(i = 0; i <= l; i++) {
			fscanf(fin, "%d", &r[l][i]);
		}
	}

	for(l = lines - 2; l >= 0; l--) {
		for(i = 0; i <= l; i++) {
			r[l][i] += max(r[l + 1][i], r[l + 1][i + 1]);
		}
	}

	FILE* fout = fopen("numtri.out", "w");
	fprintf(fout, "%d\n", r[0][0]);

	exit(0);
}