/*
ID: ruippei1
LANG: C
TASK: checker
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int n;

int s[13];

int line_occ[13];
int diag_up_occ[13+13-1];
int diag_down_occ[13+13-1];

FILE* fout;
int sols_found = 0;

void print_sol() {
	if(++sols_found > 3) {
		return;
	}
	fprintf(fout, "%d", s[0] + 1);
	int i;
	for(i = 1; i < n; i++) {
		fprintf(fout, " %d", s[i] + 1);
	}
	fprintf(fout, "\n");
}

void dfs(int col) {
	if(col >= n) {
		print_sol();
		return;
	}
	int i;
	for(i = 0; i < n; i++) {
		if(!line_occ[i] && !diag_up_occ[col+i] && !diag_down_occ[col-i+12]) {
			s[col] = i;
			line_occ[i] = diag_up_occ[col+i] = diag_down_occ[col-i+12] = 1;
			dfs(col + 1);
			line_occ[i] = diag_up_occ[col+i] = diag_down_occ[col-i+12] = 0;
		}
	}
}

int main() {
	FILE* fin = fopen("checker.in", "r");
	fscanf(fin, "%d", &n);
	fout = fopen("checker.out", "w");

	memset(line_occ, 0, sizeof(line_occ));
	memset(diag_up_occ, 0, sizeof(diag_up_occ));
	dfs(0);

	fprintf(fout, "%d\n", sols_found);
	exit(0);
}