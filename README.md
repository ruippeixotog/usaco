# Solutions to USACO Training Gateway problems

This repository contains solutions to the USACO Training Gateway problems, available at [http://ace.delos.com/usacogate][1]. These solutions are provided "as is". I give no guarantees that they will work as expected. Submitting my solutions will probably get you banned from the USACO website.

## Instructions

You can compile all the problems by issuing the following command:

    $ make

If you want to compile only a specific problem, issue the following command, replacing `<section>` and `<problem_id>` with the section and id of the problem you want to compile (see section "Problems Solved" for the list of possible ids):

    $ make <section>/<problem_id>

Running a compiled problem is just a matter of executing a command similar to the next one, replacing `1.3/barn1` with the section and id of the desired problem:

    $ 1.3/barn1

Remember that each problem expects its input in a file with a `.in` extension in the directory the binary is executed from and with name equal to the problem id, and outputs its result to a file with a `.out` extension and with name equal to the problem id.

## Problems solved

Here is a list of the problems currently in this repository. Problems marked with ✓ are done, while problems with ✗ are not complete and/or don't comply with performance requirements.

### Section 1.1

* ✓ Your Ride is Here (`ride.c`)

### Section 1.4

* ✓ Packing Rectangles (`packrec.c`)
* ✓ The Clocks (`clocks.c`)
* ✓ Mother's Milk (`milk3.c`)

### Section 1.5

* ✓ Number Triangles (`numtri.c`)
* ✓ Prime Palindromes (`pprime.c`)
* ✓ SuperPrime Rib (`sprime.c`)
* ✓ Checker Challenge (`checker.c`)

### Section 2.1

* ✓ The Castle (`castle.cpp`)
* ✓ Ordered Fractions (`frac1.c`)
* ✓ Sorting a Three-Valued Sequence (`sort3.c`)
* ✓ Healthy Holsteins (`holstein.c`)
* ✓ Hamming Codes (`hamming.c`)

### Section 2.2

* ✓ Preface Numbering (`preface.c`)
* ✓ Subset Sums (`subset.c`)
* ✓ Runaround Numbers (`runround.c`)
* ✓ Party Lamps (`lamps.cpp`)

### Section 2.3

* ✓ Longest Prefix (`prefix.cpp`)
* ✓ Cow Pedigrees (`nocows.c`)
* ✓ Money Systems (`money.cpp`)
* ✓ Controlling Companies (`concom.cpp`)

### Section 2.4

* ✓ The Tamworth Two (`ttwo.cpp`)
* ✓ Overfencing (`maze1.cpp`)
* ✓ Cow Tours (`cowtour.cpp`)
* ✓ Bessie Come Home (`comehome.cpp`)
* ✓ Fractions to Decimals (`fracdec.cpp`)

### Section 3.1

* ✓ Agri-Net (`agrinet.cpp`)
* ✓ Score Inflation (`inflate.cpp`)
* ✓ Humble Numbers (`humble.cpp`)

[1]: http://ace.delos.com/usacogate
